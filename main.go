package main

// XXX Output is corrupted depending on the replacement order

import (
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/logrusorgru/aurora"
	"gitlab.com/yoko-chance/slurp"
	"golang.org/x/term"
)

type (
	colorFunc func(interface{}) aurora.Value
	colors    map[string]colorFunc
)

var (
	c = colors{
		"black":   aurora.BgBlack,
		"white":   aurora.BgWhite,
		"red":     aurora.BgRed,
		"green":   aurora.BgGreen,
		"blue":    aurora.BgBlue,
		"cyan":    aurora.BgCyan,
		"magenta": aurora.BgMagenta,
		"yellow":  aurora.BgYellow,
	}
)

func main() {
	if term.IsTerminal(0) {
		return
	}

	dot := os.Getenv("PAINT_DOT")
	if dot == "" {
		dot = "  "
	}

	m, err := makeColors(os.Args[1:])
	if err != nil {
		log.Fatal(err)
	}

	slurp.ReadLines(os.Stdin, makeHandler(m, dot))
}

func makeHandler(m colors, dot string) slurp.Handler {
	return func(l string) error {
		var outs []string

		ss := []rune(l)
		for _, s := range ss {
			replaced := string(s)

			for t, f := range m {
				replaced = strings.Replace(replaced, t, f(dot).String(), -1)

				if replaced != string(s) {
					break
				}
			}

			outs = append(outs, replaced)
		}

		fmt.Println(strings.Join(outs, ""))

		return nil
	}
}

func makeColors(ss []string) (colors, error) {
	m := colors{}

	if len(m) <= 0 {
		m = colors{
			"0": c["black"],
			"1": c["white"],
		}

	} else if (len(ss) % 2) != 0 {
		log.Fatal("require [<<replace> <color>> ...]")
	}

	for i := 0; i < len(ss); i += 2 {
		m[ss[i]] = c[ss[i+1]]
	}

	return m, nil
}
